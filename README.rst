==================
XULRunner Releases
==================

This repository is just a stash of Mozilla XULRunner releases from http://ftp.mozilla.org/pub/mozilla.org/xulrunner/releases/

This repo helps with configuration and versioning for use with development of XUL-based applications.

Ultimately its existence is unnecessary if you have a fast internet connection and a simple scrpt...

Of course you should reference the official XULRunner homepage and distribution site for updates and other downloads.

https://developer.mozilla.org/en-US/docs/XULRunner
