#!/bin/sh

cd XUL.framework/Versions

VER=$1
PWD="`pwd`"

ln -sfn $VER Current

cd ..

ln -sfn Versions/Current/libxpcom.dylib libxpcom.dylib
ln -sfn Versions/Current/XUL XUL
ln -sfn Versions/Current/xulrunner-bin xulrunner-bin

#ln -sfn $PWD/Versions/$VER $PWD/Versions/Current

#ln -sfn $PWD/Versions/Current/libxpcom.dylib $PWD/libxpcom.dylib
#ln -sfn $PWD/Versions/Current/XUL $PWD/XUL
#ln -sfn $PWD/Versions/Current/xulrunner-bin $PWD/xulrunner-bin
